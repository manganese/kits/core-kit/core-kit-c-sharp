using System.Net.Http;

using Newtonsoft.Json;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.CoreKit;

namespace Manganese.CoreKit.Converters {
    public class HttpMethodConverterTests {
        [Test]
        public void Serialize() {
            var method = HttpMethod.Put;
            var serializedMethod = JsonConvert.SerializeObject(method, Formatting.Indented, new HttpMethodConverter());

            Assert.AreEqual(
                serializedMethod,
                @"""PUT"""
            );
        }

        [Test]
        public void Deserialize() {
            var method = JsonConvert.DeserializeObject<HttpMethod>(@"""PUT""", new HttpMethodConverter());

            Assert.AreEqual(
                method,
                HttpMethod.Put
            );
        }
    }
}
