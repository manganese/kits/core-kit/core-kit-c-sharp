using System.Net;


namespace Manganese.CoreKit {
  public class ApiError {
    public string type;
    public string message;
    public HttpStatusCode status;
  }
}