namespace Manganese.CoreKit {
  public class Version {
    public uint major;
    public uint minor;
    public uint patch;

    public Version Bump(VersionType versionType = VersionType.PATCH) {
      var bumpedVersion = new Version(this);

      switch (versionType) {
        case VersionType.MAJOR:
          bumpedVersion.major++;

          break;
        case VersionType.MINOR:
          bumpedVersion.minor++;

          break;
        case VersionType.PATCH:
          bumpedVersion.patch++;

          break;
      }

      return bumpedVersion;
    }

    public Version(Version version) {
      this.major = version.major;
      this.minor = version.minor;
      this.patch = version.patch;
    }
  }
}