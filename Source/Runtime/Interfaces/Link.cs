using System;
using System.Threading.Tasks;
using System.Net.Http;

using UnityEngine;
using UnityEngine.Networking;

using Newtonsoft.Json;

using Manganese.CoreKit.Converters;


namespace Manganese.CoreKit {
  public class Link {
    [JsonConverter(typeof(HttpMethodConverter))]
    public HttpMethod method;
    public Uri uri;

    public UnityWebRequest ToUnityWebRequest() {
      return new UnityWebRequest(this.uri, this.method.ToString());
    }

    public async Task<Texture2D> RequestTexture() {
      using (var request = UnityWebRequestTexture.GetTexture(this.uri, false)) {
        request.method = this.method.ToString();

        await request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
          throw new Exception(request.error);
        } else {
          var texture = DownloadHandlerTexture.GetContent(request);

          return texture;
        }
      }
    }

    public async Task<Texture2D> RequestAssetBundle() {
      using (var request = this.ToUnityWebRequest()) {
        request.downloadHandler = new DownloadHandlerAssetBundle(this.uri.ToString(), 0);

        await request.SendWebRequest();

        return DownloadHandlerTexture.GetContent(request);
      }
    }

    public Link(Uri uri) : this(HttpMethod.Get, uri) {}
    public Link(string uriString) : this(new Uri(uriString)) {}
    public Link(HttpMethod method, string uriString) : this(method, new Uri(uriString)) {}
    public Link(HttpMethod method, Uri uri) {
      this.method = method;
      this.uri = uri;
    }
  }
}