namespace Manganese.CoreKit {
  public enum VersionType {
    MAJOR = 3,
    MINOR = 2,
    PATCH = 1
  }
}