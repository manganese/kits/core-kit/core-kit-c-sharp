using System;
using System.Net.Http;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;


namespace Manganese.CoreKit.Converters {
  public class HttpMethodConverter : JsonConverter<HttpMethod> {
    public override void WriteJson(JsonWriter writer, HttpMethod method, JsonSerializer serializer) {
      var methodString = new JValue(method.ToString().ToUpper());

      methodString.WriteTo(writer);
    }

    public override HttpMethod ReadJson(JsonReader reader, Type objectType, HttpMethod _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        // HttpMethod is a value type and cannot be null
      } else if (reader.TokenType == JsonToken.String) {
        var methodString = (string) reader.Value;

        switch (methodString) {
          case "OPTIONS": return HttpMethod.Options;
          case "GET":     return HttpMethod.Get;
          // case "PATCH":   return HttpMethod.Patch;
          case "POST":    return HttpMethod.Post;
          case "PUT":     return HttpMethod.Put;
          case "DELETE":  return HttpMethod.Delete;
        }
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}